@extends('layouts.home')
@section('content')
<div class="container">
    <div class="portal text-center">
        <h1 class="titulo-portal">Portal Municipal da Educação e Desporto</h1>
    </div>
</div>
<div class="portal-noticias">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 noticia">
                <h1>TÍTULO</h1>  
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                <button class="btn-saiba-mais">Saiba Mais</button>
            </div>
            <div class="col-sm-6 noticia-img">
                <img src="{{ asset('/imgs/noticias/1.jpg') }}" class="img-noticias"> 
            </div>
        </div>
    </div>
</div>
<div class="porta-mapa-escolas text-center">
    <h3>Mapa de Escolas</h3>
    <div id="map" style="height: 500px; width: 100%"></div>
</div>
@endsection