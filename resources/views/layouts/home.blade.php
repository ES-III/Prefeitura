<!DOCTYPE html>
<html>
    <head>
        <title>Prefeitura de Pelotas - SMED</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{ asset('css/home.css') }}" type="text/css"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=AIzaSyDf6Sir5HWL6aNybmN9wt5ksEwSYFEA4TE"></script>
    </head>
    <body>
        <nav class="navbar navbar-expand-sm bg-white navbar-white fixed-top text-center">
            <a class="navLogo" href="#">
                <img class="logomarca" src="{{ asset('/imgs/logo.jpg') }}" alt="Logo">
            </a>
            <ul class="ul-nav">
                <li class="nav-item">
                    <a class="nav-link" href="#"><img class="icon-social" src="{{ asset('/imgs/icons/instagram.png') }}" alt="Instagram"></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><img class="icon-social" src="{{ asset('/imgs/icons/twitter.png') }}" alt="Twitter"></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><img class="icon-social" src="{{ asset('/imgs/icons/facebook.png') }}" alt="Facebook"></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><p>INÍCIO</p></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><p>NOTÍCIAS</p></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><p>DESPORTO</p></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><p>CONSELHO</p></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><p>OUTROS SERVIÇOS</p></a>
                </li>
            </ul>
        </nav>
        <div class="content">
            @yield('content')
        </div>
        <script src="{{ asset('js/maps.js')}}"></script>
        <script src="{{ asset('js/navbar-scroll.js')}}"></script>
    </body>
</html>