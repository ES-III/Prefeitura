function checkScroll(){
    var startY = $('.navbar').height() * 2;

    if ($(window).scrollTop() > startY) { 
        $('.navbar').addClass("navbar-scroll");
    } else {
        $('.navbar').removeClass("navbar-scroll");
    }
}

if ($('.navbar').length > 0) {
    $(window).on("scroll load resize", function(){
        checkScroll();
    });
}